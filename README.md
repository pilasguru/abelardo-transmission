# My configuration of transmission-daemon

Docker configuration for ROC-RK3328-CC about transmission-daemon

## Image

The image used is

* [lsioarmhf/transmission-aarch64](https://hub.docker.com/r/lsioarmhf/transmission-aarch64) 

just for **arm64** 

## Access

The acces is throught web interface on port 9091

## Obs

### Proxy

The container do not use reverse proxy

### Volumes

The volumes point to `/data` which is a mount point of external storage

